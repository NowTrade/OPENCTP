#pragma once
#include "FAStrategyBase.h"
#include "IStrategyCore_Def.h"

enum enmFAIndexs
{
	KDJ = 0,
	MACD,

};


//指标接口
class IFAIndex
{
public:
	virtual void OnBar(CFABar *p,IIndexImpl*pIndex) = 0;	//CFABar是一根完成的周期蜡烛
	virtual void OnTick(tagMarketData *p,IIndexImpl*pIndex) = 0;
};


template<typename T>
double Lowest(T t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i];
		else
			val = MIN(val, t[i]);
	}
	return val;
}

template<typename T>
double Highest(T t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i];
		else
			val = MAX(val, t[i]);
	}
	return val;
}
