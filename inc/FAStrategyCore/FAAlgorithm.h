#pragma once

// 指标算法底层库
// 注：所有指标计算中，均不考虑数组越界
class CFAAlgorithm
{
public:
	CFAAlgorithm(void);
	virtual ~CFAAlgorithm(void);

public:
  // 计算SUM（序列和）指标
  static double SUM(double* d, int M);

  // 计算MA（移动平均）指标
  static double MA(double* d, int M);

  // 计算STD（标准差）指标
  static double STD(double* d, int M);
  
};


#define MIN(A,B) (A < B ? A : B)

#define MAX(A,B) (A > B ? A : B)


