#pragma once

#include <vector>
#include <string>
#include <memory>
#include <algorithm>
#include "ctp/ThostFtdcUserApiDataType.h"
#include "ctp/ThostFtdcUserApiStruct.h"
#include "ctpimpl/Quote_def.h"
#include "FAAlgorithm.h"

#ifdef FASTRATEGYCORE_EXPORTS
#define FASTRATEGYCORE_API __declspec(dllexport)
#else
#define FASTRATEGYCORE_API __declspec(dllimport)
#endif

/************************************************************************/
/* 
	常量定义
*/
/************************************************************************/

// 策略时间维度
typedef int BAR_TYPE;

enum
{
	// 阴线（开盘价大于收盘价）
	YIN = 0,

	// 大阴线（开盘价为最高价，收盘价为最低价）
	BIGYIN = 1,

	// 阳线（开盘价小于收盘价）
	CANDLE = 2,

	// 大阳线（开盘价为最低价，收盘价为最高价）
	BIGCANDLE = 3,
};

// 策略时间维度
enum STRATEGY_TYPE
{
	TICK = 0,
	MIN,
	HOUR,
	DAY

};

const double MIN_DOUBLE = 1.e-6;

/************************************************************************/
/* 
	交易token
*/
/************************************************************************/
// 下单/撤单的key
class COrderKey
{
public:
	std::string m_brokerID;
	std::string m_investorID;
	int m_frontID;
	int m_sessionID;
	std::string m_orderRef;
	std::string m_instrumentID;
	double m_price;

public:
	bool operator< (const COrderKey& rhs) const;
	bool operator== (const COrderKey& rhs) const;
};

/************************************************************************/
/* 
	派生一个基于vector的类，该类的[0]为原始vector的最后一个元素, [1]为倒数第二个，以此类推
*/
/************************************************************************/

template<class _Ty>
class CFAVector: public std::vector<_Ty>
{
public:
	reference operator[](size_type _Pos)
	{
		return __super::operator[](size() - _Pos - 1);
	}
};

/************************************************************************/
/* 
	一根K线
*/
/************************************************************************/

class CFABar
{
public:	
	double open;			// 开	
	double high;			// 高
	double low;				// 低	
	double close;			// 收
	double turnover;		// 成交额（现额）	
	int volume;				// 成交量（现量）	
	std::string st;			// bar的开始时间（交易所时间）	
	std::string et;			// bar的截止时间（交易所时间）	
	int tick_counter;		// 合成该bar的tick计数
public:

	// 阴线（开盘价大于收盘价）
	// 大阴线（开盘价为最高价，收盘价为最低价）
	// 阳线（开盘价小于收盘价）
	// 大阳线（开盘价为最低价，收盘价为最高价）
	BAR_TYPE Type() const
	{
		if (fabs(open - high) < MIN_DOUBLE && fabs(close - low) < MIN_DOUBLE)
			return BIGYIN;

		if (fabs(open - low) < MIN_DOUBLE && fabs(close - high) < MIN_DOUBLE)
			return BIGCANDLE;

		if (open > close)
			return YIN;

		return CANDLE;
	}
public:
	CFABar()
	{
		open = high = low = close = turnover = 0;
		volume = 0;
		tick_counter = 0;
	}
	~CFABar(){}
};

typedef CFAVector<tagMarketData> CTP_MARKETDATA_QUEUE;

typedef CFAVector<CFABar> BAR_QUEUE;

typedef CFAVector<double> BAR_KDJ;

template<class _Ty>
class CProperty
{
public:
	virtual double operator[](int _Pos) = 0;

	BAR_QUEUE::size_type size() const
	{
		return m_arr->size();
	}

public:
	_Ty* m_arr;
};


class CBarProperty : public CProperty<BAR_QUEUE>
{

};

class CTickProperty : public CProperty<CTP_MARKETDATA_QUEUE>
{

};

class COpen : public CBarProperty
{
public:
	double operator[](int _Pos)
	{
		return (*m_arr)[_Pos].open;
	}
};

class CHigh : public CBarProperty
{
public:
	double operator[](int _Pos)
	{
		return (*m_arr)[_Pos].high;
	}
};

class CLow : public CBarProperty
{
public:
	double operator[](int _Pos)
	{
		return (*m_arr)[_Pos].low;
	}
};

class CClose : public CBarProperty
{
public:
	double operator[](int _Pos)
	{
		return (*m_arr)[_Pos].close;
	}
};

class CNew : public CTickProperty
{
public:
	double operator[](int _Pos)
	{
		return (*m_arr)[_Pos].dLastPrice;
	}
};