#pragma once
#include "FAStrategyBase.h"
#include "IFAIndex.h"

/************************************************************************/
/* 
	指标集合
*/
/************************************************************************/

	
// 策略参数
class CFAIndexParams
{
public:
	CFAIndexParams();
	virtual ~CFAIndexParams(){}
public:
	std::string m_strategyName;			// 策略名称	
	std::string m_instrument;			// 合约代码	
	STRATEGY_TYPE m_strategyType;		// 策略时间维度	
	int m_timeInterval;					// 时间数 （与m_strategyType结合使用）	
	int m_units;						// 默认开平仓手数	
	double m_minMove;					// 合约的最小跳数（用于控制滑点）	
	double m_tickPrice;					// 每点价值	
	double m_fee;						// 手续费	
	int m_longs;						// 多头头寸	
	int m_shorts;						// 空头头寸
public:	
	double m_dOpenLongPrice;			// 多头开仓价	
	double m_dOpenShortPrice;			// 空头开仓价
	std::string m_brokerID;				// 账户参数
	std::string m_investorID;
	std::string m_frontID;
	std::string m_sessionID;
};


//指标基础
class CFAIndex:public IFAIndex
{
public:
	CFAIndex();
	virtual~CFAIndex(){}
public:
	virtual void OnBar(CFABar *p, IIndexImpl*pIndex);
	virtual void OnTick(tagMarketData *p, IIndexImpl*pIndex);

public:

public:
	// 注：序列化数据，0代表bar最新价，1代表前一根bar的收盘价，依次类推

	COpen	Open;		// 开盘价
	CHigh	High;		// 最高价
	CLow	Low;		// 最低价
	CClose	Close;		// 收盘价
	CNew	New;		// 最新价,tick数据
	double	Last;		// 当前最新价

	// bar数据（只存储已形成的bar）
	BAR_QUEUE BarQueue;
	// tick数据
	CTP_MARKETDATA_QUEUE TickQueue;
};


