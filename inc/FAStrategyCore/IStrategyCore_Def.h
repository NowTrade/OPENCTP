#pragma once
#include "FAStrategyBase.h"

class IStrategyCore
{
public:
	virtual void OnBar(CFABar *p) = 0;
	virtual void OnTick(tagMarketData *p) = 0;

};

class IIndexImpl
{
public:
	virtual void OnKDJ(BAR_KDJ &k, BAR_KDJ&d, BAR_KDJ&j) = 0;
};