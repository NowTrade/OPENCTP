
#pragma once

#include "Proxy_def.h"
#include "ctp/ThostFtdcTraderApi.h"

//-------------------------------------------------------------------------------------------
// 交易数据类型
//-------------------------------------------------------------------------------------------

// 交易类型(投机, 套利, 套保)
enum enTradeType
{
	TRADE_TYPE_SPECULATION                          = 0,                                    // 投机
	TRADE_TYPE_ARBITRAGE                            = 1,                                    // 套利
	TRADE_TYPE_HEDGE                                = 2,                                    // 套保
};

// 交易买卖方向类型(买, 卖)
enum enTradeDir
{
	TRADE_DIR_BUY                                   = 0,                                    // 买
	TRADE_DIR_SELL                                  = 1,                                    // 卖
};

// 交易开平类型
enum enTradeOperate
{
	TRADE_OPERATE_OPEN                              = 0,                                    // 开仓
	TRADE_OPERATE_CLOSE                             = 1,                                    // 平仓
	TRADE_OPERATE_CLOSE_TODAY                       = 2,                                    // 平今
	TRADE_OPERATE_EXCUTE                            = 3,                                    // 期权行权
};

// 交易报单类型
enum enTradeOrderType
{
	TRADE_ORDER_TYPE_LIMIT                          = 0,                                    // 限价单
	TRADE_ORDER_TYPE_MARKET                         = 1,                                    // 市价单
	TRADE_ORDER_TYPE_FAK                            = 2,                                    // 部成即撤
	TRADE_ORDER_TYPE_FOK                            = 3,                                    // 全成全撤
};

// 交易报单状态
enum enTradeOrderStatus
{
	TRADE_ORDER_STATUS_NORMAL                       = 0,                                    // 委托
	TRADE_ORDER_STATUS_PARTIAL                      = 1,                                    // 部分成交
	TRADE_ORDER_STATUS_TRADED                       = 2,                                    // 全部成交
	TRADE_ORDER_STATUS_CANCELED                     = 3,                                    // 撤单
};

// 交易商品类型
enum enProductType
{
	PRODUCT_TYPE_FUTURES                            = 0,                                    // 期货
	PRODUCT_TYPE_OPTIONS                            = 1,                                    // 期货期权
	PRODUCT_TYPE_COMBINATION                        = 2,                                    // 组合
	PRODUCT_TYPE_SPOT                               = 3,                                    // 即期
	PRODUCT_TYPE_EFP                                = 4,                                    // 期转现
	PRODUCT_TYPE_SPOT_OPTION                        = 5,                                    // 现货期权
};

//-------------------------------------------------------------------------------------------
// 交易数据项
//-------------------------------------------------------------------------------------------

// 结算单
struct tagSettlement
{
	char                                            szTradingDay[DATE_LENGTH];              // 交易日
	int                                             nSettlementID;                          // 结算编号
	char                                            szBrokerID[BROKER_ID_LENGTH];           // 经纪ID
	char                                            szInvestorID[INVESTOR_ID_LENGTH];       // 投资者ID
	int                                             nSequenceNo;                            // 序号
	char                                            szContent[CONTENT_LENGTH];              // 消息正文
};

// 帐户权益数据项
struct tagTradingAccount
{
	double                                          dPreBalance;                            // 上次结算准备金
	double                                          dPositionProfit;                        // 持仓盈亏
	double                                          dCloseProfit;                           // 平仓盈亏
	double                                          dCommission;                            // 手续费
	double                                          dCurrentMargin;                         // 当前保证金总额
	double                                          dFrozenCapital;                         // 冻结资金
	double                                          dAvaiableCapital;                       // 可用资金
	double                                          dDynamicEquity;                         // 动态权益
};

// 报单数据项
struct tagOrder
{
	int												nSessionID;								// SessionID
	long                                            nOrderID;                               // 本地报单ID
	long											nOrderSysID;							// SystemID

	char                                            szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	char                                            szExchangeID[EXCHANGE_ID_LENGTH];       // 交易所ID
	enTradeType                                     nTradeType;                             // 交易类型(投机,套利,套保)
	enTradeDir                                      nTradeDir;                              // 交易方向(买,卖)
	enTradeOperate                                  nTradeOperate;                               // 交易开平操作(开,平,平今)
	enTradeOrderStatus                              nOrderStatus;                           // 报单状态

	double                                          dLimitPrice;                            // 报价
	double                                          dAvgPrice;                              // 成交均价
	int                                             nVolume;                                // 报单数量
	int                                             nTradeVolume;                           // 本次成交量
	int                                             nTradeVolumeLeft;                       // 本次未成交量

	char                                            szInsertDateTime[DATETIME_LENGTH];      // 委托时间(交易所)
	char                                            szTradeDateTime[DATETIME_LENGTH];       // 最后成交时间

	char                                            szOrderRefCustom[13];                    // 客户自定义自动(xSpeed仅支持数字)
};

// 成交数据项
struct tagTrade
{
	char                                            szTradeID[TRADE_ID_LENGTH];             // 成交编号
	long                                            nOrderID;                               // 对应委托报单ID
	long                                            nSystemID;								// SystemID

	char                                            szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	char                                            szExchangeID[EXCHANGE_ID_LENGTH];       // 交易所ID
	enTradeType                                     nTradeType;                             // 交易类型(投机,套利,套保)
	enTradeDir                                      nTradeDir;                              // 交易方向(买,卖)
	enTradeOperate                                  nTradeOperate;                          // 交易开平操作(开,平,平今)

	double                                          dPrice;                                 // 价格
	int                                             nVolume;                                // 数量

	char                                            szTradeTime[DATETIME_LENGTH];           // 成交时间
	char                                            szTradingDay[DATETIME_LENGTH];          // 交易日
};

// 持仓数据项
struct tagPosition
{
	char                                            szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	enTradeType                                     nTradeType;                             // 交易类型(投机,套利,套保)
	enTradeDir                                      nTradeDir;                              // 交易方向(买,卖)

	double                                          dAvgPrice;                              // 持仓均价
	int                                             nPosition;                              // 总持仓量
	int                                             nYesterdayPosition;                     // 昨仓
	int                                             nTodayPosition;                         // 今仓
	double                                          dMargin;                                // 占用保证金
};

// 持仓明细数据项
struct tagPositionDetail
{
	char                                            szBrokerID[BROKER_ID_LENGTH];           // 经纪ID
	char                                            szInvestorID[INVESTOR_ID_LENGTH];       // 投资者ID

	char                                            szTradeID[TRADE_ID_LENGTH];             // 成交编号
	char                                            szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	char                                            szExchangeID[EXCHANGE_ID_LENGTH];       // 交易所ID
	enTradeType                                     nTradeType;                             // 交易类型(投机,套利,套保)
	enTradeDir                                      nTradeDir;                              // 交易方向(买,卖)    
	double                                          dOpenPrice;                             // 开仓价
	int                                             nVolume;                                // 数量(手数)
	char                                            szOpenDate[DATE_LENGTH];                // 开仓日期

	/////交易日
	//TThostFtdcDateType	TradingDay;    
	/////结算编号
	//TThostFtdcSettlementIDType	SettlementID;
	/////成交类型
	//TThostFtdcTradeTypeType	TradeType;
	/////组合合约代码
	//TThostFtdcINSTRUMENT1IDType	CombINSTRUMENT1ID;
	/////逐日盯市平仓盈亏
	//TThostFtdcMoneyType	CloseProfitByDate;
	/////逐笔对冲平仓盈亏
	//TThostFtdcMoneyType	CloseProfitByTrade;
	/////逐日盯市持仓盈亏
	//TThostFtdcMoneyType	PositionProfitByDate;
	/////逐笔对冲持仓盈亏
	//TThostFtdcMoneyType	PositionProfitByTrade;
	/////投资者保证金
	//TThostFtdcMoneyType	Margin;
	/////交易所保证金
	//TThostFtdcMoneyType	ExchMargin;
	/////保证金率
	//TThostFtdcRatioType	MarginRateByMoney;
	/////保证金率(按手数)
	//TThostFtdcRatioType	MarginRateByVolume;
	/////昨结算价
	//TThostFtdcPriceType	LastSettlementPrice;
	/////结算价
	//TThostFtdcPriceType	SettlementPrice;
	/////平仓量
	//TThostFtdcVolumeType	CloseVolume;
	/////平仓金额
	//TThostFtdcMoneyType	CloseAmount;
};

// 合约信息项
struct tagInstrument
{
	char                                            szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	char                                            szProductID[32];                        // 产品ID
	char                                            szExchangeID[8];                        // 交易所ID
	int                                             nMultiple;                              // 合约数量乘数
	double                                          dMinMove;                               // 最小变动价格
};


//报单
struct stOrderInfo
{
	int						nSession;
	char					szExchangeID[9];						
	char                    szINSTRUMENT1ID[INSTRUMENT1_ID_LENGTH];   // 合约ID
	long					lOrderLocalID;
	long					lOrderSysID;
	enTradeDir				tradeDir;	
	enTradeOperate			tradeOperate;
	enTradeOrderStatus		tradeStatus;
	double					dLimitPrice;
	int						nVolume;
	char					OrderRef[13];							//报单引用
};

//交易
struct stTradeInfo
{
	long					lTradeLocalID;
	long					lTradeSysID;
	enTradeDir				tradeDir;	
	enTradeOperate			tradeOperate;
	double					dPrice;
	int						nVolume;
};