#pragma once
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#define _FTA(x) x
#define _FTW(x) L#x

#ifdef _UNICODE
#define _FFT(x) _FTW(x)
#else
#define _FFT(x) _FTA(x)
#endif

//--------------------------------------------------------------------------------
// 类型定义
//--------------------------------------------------------------------------------

enum
{
	SUCCESS = 0,
	FAIL,
	FATAL,
};

// PROXY API类型
enum enProxyAPIType
{
	PROXY_API_TYPE_CTP          = 0,                // CTP
	PROXY_API_TYPE_FEMAS        = 1,                // 飞马
	PROXY_API_TYPE_XSPEED       = 2,                // 飞创
	PROXY_API_TYPE_JSD          = 3,                // 金仕达
};

//--------------------------------------------------------------------------------
// 常量定义
//--------------------------------------------------------------------------------

#define PROXY_PRODUCT_FLAG      "@YunXiang"         // 产品信息标志
#define PROXY_LOG_PATH          "./log/"            // 日志目录

//--------------------------------------------------------------------------------
// 长度定义
//--------------------------------------------------------------------------------

#define BROKER_ID_LENGTH        11                  // 经纪ID长度
#define INVESTOR_ID_LENGTH      16                  // 投资人ID长度
#define PASSWORD_LENGTH         41                  // 密码长度
#define INSTRUMENT1_ID_LENGTH    31                  // 合约ID长度
#define EXCHANGE_ID_LENGTH      9                   // 交易所ID
#define TRADE_ID_LENGTH         21                  // 成交编号

#define ERROR_MSG_LENGTH        81                  // 错误消息长度
#define CONTENT_LENGTH          501                 // 正文长度

#define DATE_LENGTH             9                   // 日期长度
#define TIME_LENGTH             9                   // 时间长度
#define DATETIME_LENGTH         16                  // 日期时间长度

#define URL_LEN					128					//服务器地址长度

//打印，日志输出

#define LOG_DEBUG "DEBUG"
#define LOG_TRACE "TRACE"
#define LOG_ERROR "ERROR"
#define LOG_INFO  "INFOR"
#define LOG_CRIT  "CRTCL"

#define OUTPUT(x) printf(x);printf("\n");
#define LOG(msg)
#define OUTPUT2(level, format, ...) \
	do {\
			printf("[%s|%s,%d] " format "\n", \
				level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
			if (level == LOG_ERROR || level == LOG_INFO) {	\
			char sz[512] = { '\0' }; \
			sprintf(sz,"[%s|%s,%d] " format, \
				level, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
			LOG(sz) }; \
		} while (0)


#define INS_MAX_COUNT 128			//最大128个合约订阅