#pragma once

#include "FAQuote/FAQuote_Def.h"
#include "FATrade/FATrade_Def.h"
#include "ctpimpl/Quote_i.h"
#include "ctpimpl/Trade_i.h"
#include <vector>
using namespace std;

#define ARRAY_SIZE				128					//策略最大个数

#define PLATFORM_INS_MAX_COUNT	3					//支持套利+单边,3个不同合约的订阅行情

//策略接口
class IStrategyPlus
{
public:
	virtual int Init() = 0;
	virtual int UnInit() = 0;
	virtual int OnQuote(tagMarketData * pTick) = 0;
	virtual int OnOrderStatus() = 0;
	virtual int OnTradeStatus() = 0;
};

typedef IStrategyPlus* LPSTRATEGYPLUS;


class IStrategyPlatform
{
public:
	IStrategyPlatform(){}
	virtual ~IStrategyPlatform(){}
protected:
	IStrategyPlatform(const IStrategyPlatform&);
	IStrategyPlatform& operator=(const IStrategyPlatform&);
public:
	//初始化ctp接口
	virtual int Init() = 0;
	virtual int UnInit() = 0;

	//订阅
	virtual int SubInstrument(char*szIns[], int &nCount) = 0;

	//sqlite
	virtual int LoadTick(char**szIns,int &nCount,char *dateTime) = 0;
	virtual int PlayHistoryTick() = 0;

};