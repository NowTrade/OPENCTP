// Demo2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

/*
	策略主程序
*/


#include "dllHelper.h"
#include "FAQuote/FAQuote_Def.h"
#include "FATrade/FATrade_Def.h"
#include "StrategyPlatform/StrategyPlatform_Def.h"

#ifdef _DEBUG
CDllHelper _dll("StrategyPlatformD.dll");
#else
CDllHelper _dll("StrategyPlatform.dll");
#endif

typedef IStrategyPlatform* (*CreateStrategyPlatform)();
CreateStrategyPlatform _func = NULL;
IStrategyPlatform * _pPlatForm = NULL;


int _tmain(int argc, _TCHAR* argv[])
{
	_func = _dll.GetProcedure<CreateStrategyPlatform>("CreateObject");
	if (_func)
	{
		_pPlatForm = _func();
		printf("get func\n");
	}

	_pPlatForm->Init();

	getchar();
	return 0;
}

