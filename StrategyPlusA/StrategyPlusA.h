
/************************************************************************/
/* 
	简单策略：
	1.5分钟均价，开仓
	2. 5个点，止盈止损
	
*/
/************************************************************************/


#include "StrategyPlatform/StrategyPlatform_Def.h"
#include "StrategyPlusACfg.h"
#include "StrategyPlusA_Def.h"
#include "FAStrategyCore/IStrategyCore_Def.h"

#ifdef STRATEGYPLUSA_EXPORTS
#define STRATEGYPLUSA_API __declspec(dllexport)
#else
#define STRATEGYPLUSA_API __declspec(dllimport)
#endif

/*
	分钟K线（tick构成candle的方法）

	1.自实现，规则不同，K线不同。
	2.举例分钟K线的形成

*/

#define MIN_TICK	(1000*60)

class IMinBar
{
public:
	virtual void OnCreateBar(CFABar *p) = 0;
};

struct tagTickA
{
	tagMarketData data;
	int tickcount;
};

typedef CFAVector<tagTickA> TICKA;

class CMinBarA
{
public:
	CMinBarA();
	virtual ~CMinBarA(){}

public:
	void Init(IMinBar * p);
	void OnTick(tagMarketData * p);
	int CreateBar(CFABar &bar);

private:
	double Lowest(TICKA &t, int len);
	double Highest(TICKA &t, int len);
	int Volumeest(TICKA &t, int len);

private:
	TICKA	m_tick;
	IMinBar	* m_pMinBar;
};


//策略实例
class CStrategyPlusA :public IStrategyPlus,public IIndexImpl,public IMinBar{
public:
	CStrategyPlusA();
	CStrategyPlusA(IStrategyPlatform*pPlatform);
	// TODO:  在此添加您的方法。

	virtual int Init();
	virtual int UnInit() { return SUCCESS; }
	virtual int OnQuote(tagMarketData * pTick);
	virtual int OnOrderStatus() { return SUCCESS; }
	virtual int OnTradeStatus() { return SUCCESS; }

	//获取相关指标
	virtual void OnKDJ(BAR_KDJ &k, BAR_KDJ&d, BAR_KDJ&j);

	//分钟K线形成
	virtual void OnCreateBar(CFABar * p);


public:
	bool CanOrder();
private:
	CStrategyPlusACfg m_cfg;
	IStrategyPlatform * m_pPlatform;
	IStrategyCore * m_pCore;
private:

	tagStrategyPlusAParams	m_params;
	CMinBarA				m_minBarA;
};

STRATEGYPLUSA_API IStrategyPlus* CreateObject(IStrategyPlatform*pPlatform);
STRATEGYPLUSA_API int ReleaseObject(void);
