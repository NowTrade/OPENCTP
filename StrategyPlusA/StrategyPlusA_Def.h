#pragma once

//策略A基础参数

enum ORDER_STATUS
{
	ORDER_REPORT_INSERT,	//订单上报
	ORDER_SUSPEND,			//挂单
	ORDER_TRADED,			//成交
	ORDER_REPORT_CANCEL,	//撤单上报
	ORDER_CANCEL,			//撤单

};

typedef void(*ParamsFunc)(ORDER_STATUS);

struct  tagStrategyPlusAParams
{
	int firstTickCount;
	int position;
	float price;
	ParamsFunc func;
};
