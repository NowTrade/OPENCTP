#include "stdafx.h"
#include "StrategyPlusA.h"
#include "dllHelper.h"

CMinBarA::CMinBarA()
:m_pMinBar(0)
{

}

void CMinBarA::Init(IMinBar * p)
{
	m_pMinBar = p;
}

double CMinBarA::Lowest(TICKA &t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i].data.dLastPrice;
		else
			val = MIN(val, t[i].data.dLastPrice);
	}
	return val;
}

double CMinBarA::Highest(TICKA &t, int len)
{
	double val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		if (0 == i)
			val = t[i].data.dLastPrice;
		else
			val = MAX(val, t[i].data.dLastPrice);
	}
	return val;
}


int CMinBarA::Volumeest(TICKA &t, int len)
{
	int val = 0;
	int len_u = len < t.size() ? len : t.size();
	for (int i = 0; i < len_u; ++i)
	{
		val += t[i].data.nVolume;
	}
	return val;
}


void CMinBarA::OnTick(tagMarketData * p)
{
	tagTickA a;
	memcpy(&a, p, sizeof(tagMarketData));
	a.tickcount = GetTickCount();
	m_tick.push_back(a);
	CFABar Bar;
	if (SUCCESS == CreateBar(Bar))
	{
		m_pMinBar->OnCreateBar(&Bar);
		//一个bar形成清空
		m_tick.clear();
	}
}


int CMinBarA::CreateBar(CFABar &Bar)
{
	/************************************************************************/
	/* 
		tick 转 candle的算法
		1. 简略算法，时间戳差值>=60s
	*/
	/************************************************************************/
	for (int i = 1; i < m_tick.size();i++)
	{
		if (m_tick[0].tickcount - m_tick[i].tickcount >= MIN_TICK)
		{
			Bar.open = m_tick[i].data.dLastPrice;
			Bar.close = m_tick[0].data.dLastPrice;
			Bar.high = Lowest(m_tick,i);
			Bar.low = Highest(m_tick,i);
			Bar.volume = Volumeest(m_tick, i);
			Bar.tick_counter = i + 1;
			return SUCCESS;
		}
	}


	return FAIL;
}

/************************************************************************/
/* 

*/
/************************************************************************/

//加载策略核心

#ifdef _DEBUG
CDllHelper _dll(_FTA("FAStrategyCoreD.dll"));
#else
CDllHelper _dll(_FTA("FAStrategyCoreD.dll"));
#endif

typedef IStrategyCore* (*CreateStrategyCore)(IIndexImpl*pIndex);
typedef int (*ReleaseStrategyCore)(IStrategyCore*p);

CStrategyPlusA::CStrategyPlusA()
:m_pCore(0)
, m_pPlatform(0)
{
	m_minBarA.Init(this);
}

CStrategyPlusA::CStrategyPlusA(IStrategyPlatform*pPlatform)
:m_pPlatform(pPlatform)
,m_pCore(0)
{
	m_minBarA.Init(this);
}

int CStrategyPlusA::Init()
{
	m_cfg.Load();
	char *p[INS_MAX_COUNT] = { 0 };
	m_cfg.GetIns(p);
	int nCount = m_cfg.GetInsCount();
	if (!m_pPlatform)
		return FAIL;
	
	m_pPlatform->SubInstrument(p,nCount);
	
	CreateStrategyCore _func;
	_func = _dll.GetProcedure<CreateStrategyCore>("CreateObject");
	if (!_func)
		return FAIL;
	
	ReleaseStrategyCore _func2;
	_func2 = _dll.GetProcedure<ReleaseStrategyCore>("ReleaseObject");
	if (!_func2)
		return FAIL;
	
	m_pCore = _func(this);
	

	return SUCCESS;
}

int CStrategyPlusA::OnQuote(tagMarketData * pTick)
{
	m_pCore->OnTick(pTick);
	m_minBarA.OnTick(pTick);
	return SUCCESS;
}

void CStrategyPlusA::OnKDJ(BAR_KDJ &k, BAR_KDJ&d, BAR_KDJ&j)
{
	//接受KDJ更新
	if (k.size() < 10)
	{
		return;
	}

	//KDJ金叉做多
}

void CStrategyPlusA::OnCreateBar(CFABar * p)
{
	m_pCore->OnBar(p);
}