#pragma once

#include "StrategyPlatform/StrategyPlatform_Def.h"


#define PLUS_CFG_XML	_FTA("StrategyPlus.xml")

struct tagOpen
{
	int		nPosition;
	float	fJumpPoint;
	int		nPointRMB;
	float	fSlippage;
	int		nCancelTime;

};

struct tagCover
{
	int		nWin;
	int		nLose;
};

struct tagPlusCfgA
{
	int nInsCount;
	char szIns[PLATFORM_INS_MAX_COUNT][INSTRUMENT1_ID_LENGTH];
	tagOpen tOpen;
	tagCover tCover;
};

class CStrategyPlusACfg
{
public:
	CStrategyPlusACfg();
	~CStrategyPlusACfg();

	tagPlusCfgA&GetCfg() { return m_cfg; }

	char **GetIns(char *p[]);
	int GetInsCount(){ return m_cfg.nInsCount; }

public:
	int Load();
	int  ReLoad();

private:
	tagPlusCfgA m_cfg;
};

