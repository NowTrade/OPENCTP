#include "stdafx.h"
#include "StrategyPlusACfg.h"

#include "tinyxml/tinyxml.h"

#ifdef _DEBUG
#pragma  comment(lib,"../lib/tinyxml_d.lib")
#else
#pragma  comment(lib,"../lib/tinyxml.lib")

#endif

CStrategyPlusACfg::CStrategyPlusACfg()
{
}


CStrategyPlusACfg::~CStrategyPlusACfg()
{
}

int CStrategyPlusACfg::Load()
{
	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(PLUS_CFG_XML, TIXML_ENCODING_UTF8))
		return FAIL;

	TiXmlElement * node = xmlDoc.FirstChildElement("StrategyPlusA");
	if (!node)
		return FATAL;

	TiXmlElement *node1 = node->FirstChildElement("Instrument");
	if (!node1)
		return FATAL;

	if (!node1->Attribute("num"))
		return FAIL;
	m_cfg.nInsCount = atoi(node1->Attribute("num"));
	string sz(node1->Attribute("num"));
	string sz2(node1->Attribute("Ins1"));
	for (int i = 0; i < m_cfg.nInsCount && i < PLATFORM_INS_MAX_COUNT; i++)
	{
		char sz[33];
		sprintf_s(sz, 33, "Ins%d", i + 1);
		sprintf_s(m_cfg.szIns[i], INSTRUMENT1_ID_LENGTH, node1->Attribute(sz));
	}

	node1 = node->FirstChildElement("Open");
	if (!node1)
		return FATAL;
	m_cfg.tOpen.nPosition = atoi(node1->Attribute("position"));
	m_cfg.tOpen.fJumpPoint = atof(node1->Attribute("jump"));
	m_cfg.tOpen.nPointRMB = atoi(node1->Attribute("PointRMB"));
	m_cfg.tOpen.fSlippage = atof(node1->Attribute("Slippage"));
	m_cfg.tOpen.nCancelTime = atoi(node1->Attribute("CancelTime"));

	node1 = node->FirstChildElement("Cover");
	if (!node1)
		return FATAL;
	m_cfg.tCover.nWin = atoi(node1->Attribute("win"));
	m_cfg.tCover.nLose = atoi(node1->Attribute("lose"));

	return SUCCESS;
}

char **CStrategyPlusACfg::GetIns(char *p[])
{
	for (size_t i = 0; i < m_cfg.nInsCount; i++)
	{
		p[i] = m_cfg.szIns[i];
	}

	return p;
}