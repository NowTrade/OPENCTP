xcopy  /y /e /s "..\FutureUp\inc\*" ".\inc"  

xcopy  /y /e /s "..\FutureUp\doc\*" ".\doc"  

xcopy  /y "..\FutureUp\bin\FAQuote.dll" ".\bin" 
xcopy  /y "..\FutureUp\bin\FAQuoteD.dll" ".\bin" 

xcopy  /y "..\FutureUp\bin\FATrade.dll" ".\bin" 
xcopy  /y "..\FutureUp\bin\FATradeD.dll" ".\bin" 

xcopy  /y "..\FutureUp\bin\FAStrategyCoreD.dll" ".\bin" 
xcopy  /y "..\FutureUp\bin\FAStrategyCore.dll" ".\bin" 

xcopy  /y "..\FutureUp\bin\StrategyPlatform.dll" ".\bin" 
xcopy  /y "..\FutureUp\bin\StrategyPlatformD.dll" ".\bin" 

xcopy  /y "..\FutureUp\bin\StrategyPlus.xml" ".\bin"

xcopy  /y /e /s "..\FutureUp\StrategyPlusA\*" ".\StrategyPlusA"  
xcopy  /y /e /s "..\FutureUp\Demo2\*" ".\Demo2"  

pause